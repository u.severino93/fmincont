// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2021 - Stephane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

function fmincon_fit()

function f = costf(x, _t, _y)
    z=x(1)*cos(x(2)*_t+x(3)).*exp(x(4)*_t.^2);
    r=z-_y;
    f=sum(r.*r);    
endfunction

function [c,ceq,dc,dceq] = constr(x)
    c = norm(x)^2-6;
    ceq = [];
    if argn(1)==4 then
        dc = 2*x;
        dceq = [];
    end
endfunction

function plotfit(x,optimValues,state,_t,_y)
    z=x(1)*cos(x(2)*_t+x(3)).*exp(x(4)*_t.^2);
    if state == "init" then
        drawlater
        clf
        hdl = plot(_t,z,_t,_y,'o')
        hdl(2).tag = "fmincon_curve";
        gca().data_bounds(3:4)=[-1.5,1.5];
        legend model data
        drawnow
    elseif state == "iter"
        hdl = findobj("tag","fmincon_curve");
        hdl.data(:,2) = z;
    elseif state == "done"
        title(msprintf("Final residual is %f",optimValues.fval))
    end
endfunction

//rand("seed",7);
t=linspace(-10,10,100);
y=cos(2*t+1).*exp(-0.1*t^2);
y=y+rand(y,'normal')/10;

opt =  optimoptions("fmincon",...
"SpecifyConstraintGradient",1,...
"Display","iter",...
"PlotFcn",list(plotfit,t,y),...
"FiniteDifferenceType","complexstep",...
"OptimalityTolerance",1e-8);

problem = struct();
problem.x0 = [0;0;0;0];
problem.objective = list(costf,t,y);
problem.nonlcon = constr;
problem.lb = [0, 0, 0, -%inf];
problem.ub = [%inf, %inf, %inf, 0];
problem.options = opt;

[x,fval,exitflag,output,lambda] = fmincon(problem);

demo_viewCode("fmincon_fit.sce")
endfunction

fmincon_fit()
clear fmincon_fit






