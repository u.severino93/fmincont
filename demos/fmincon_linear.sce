// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt


function fmincon_linear()

//   Use a modified Rosenbrock's Post Office Problem.
//   Uses 2 variables instead of 3, which allows to make plots.
//   Set x3 to 12, constantly.
//   
//

mprintf("\n\n==============================\n");
mprintf("Illustrates fmincon algorithm on a modified Rosenbrock''s Post Office Problem.\n");
mprintf("This problems has a nonlinear objective, with linear constraints.\n");
mprintf("Defining Rosenbrock Post Office function...\n");


//
//  Reference:
//
//    An automatic method for finding the
//    greatest or least value of a function
//    Rosenbrock
//    1960
//

//
// Test fmincon / interior-point with a linearily constrained problem.
//
// Find values of x that minimize f(x) = - x1 * x2, starting at the point x0 = [1;1], 
// subject to the constraints:
// 0 <= x1 + 2x2 <= 48
//
// This constraint comes from 
// 0 <= x1 + 2x2 + 2x3 <= 72 
// with x3=12.
//
// The solution is x*=[24;12].

function f = objfun ( x )
  f = - prod(x)
endfunction

// Plot the objective function
function f = objfunC ( x1 , x2 )
  f = objfun ([x1 x2])
endfunction

clf
xopt = [24;12];
// Plot the optimum 
mprintf("X* is the red dot.\n");
plot(xopt(1),xopt(2),"r.")


NP = 100;
x = linspace(0,30,NP);
y = linspace(-10,40,NP);
contour(x,y,objfunC,10);
// Plot constraint #1 : x2 >= -x2/2-12
plot(x,-x/2-12);
h=gcf();
h.children.children(1).children.thickness = 4;
// Plot constraint #1 : x2 <= 24-x2/2
plot(x,24-x/2);
h=gcf();
h.children.children(1).children.thickness = 4;

mprintf("The feasible set is between the two thick blue lines.\n");

/////////////////
A = [
  -1 -2
   1  2
];
b = [
  0
  48
];

function f = objfunP ( x )
  f = - prod(x)
  plot(x(1),x(2),"g*")
endfunction

// Initial guess
x0 = [1;1];

// Plot the x0 
mprintf("X0 is the blue dot.\n");
plot(x0(1),x0(2),"b.")

xcomp = fmincon (objfunP,x0,A,b);
xcomp=xcomp(:);
mprintf("Solution is the red dot.\n");
plot(xcomp(1),xcomp(2),"r.")

mprintf("x computed=[%s]\n",strcat(string(xcomp)," "));
shift = norm(xcomp-xopt)/norm(xopt);
mprintf("x expected=[%s] (Relative Shift =%e)\n",strcat(string(xopt)," "),shift);
endfunction

fmincon_linear()
clear fmincon_linear

