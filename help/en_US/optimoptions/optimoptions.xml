<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Ajouter ici d'Ã©ventuels commentaires sur le fichier XML
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns4="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:id="optimoptions" xml:lang="en">
    <refnamediv>
        <refname>optimoptions</refname>
        <refpurpose>Configures and returns an optimization data structure.</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Syntax</title>
        <synopsis>
            options = optimoptions(funname)
            options = optimoptions(funname, key1, value1, key2, value2, ...)
            options = optimoptions(oldoptions , ...)
        </synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Description</title>
        <para>
            This function creates or updates a data structure which can
            be used or modify the behaviour of optimization methods.
            The goal of this function is to manage the "options" data structure,
            which is a list with a set of fields (for example, "MaxIterations", "MaxFunctionEvaluations", etc...).
            The user can create a new structure with empty fields or create
            a new structure with default fields which correspond to a particular
            algorithm.
            The user can also configure each field and set it to a particular value.
            Finally, the user pass the structure to an optimization function
            so that the algorithm uses the options configured by the user.
        </para>
        <para>
            In the following, we analyse the various ways to call the optimoptions
            function.
        </para>
        <para>The following syntax</para>
        <programlisting role='no-scilab-exec'><![CDATA[
options = optimoptions(funname)
 ]]></programlisting>
        <para>creates a new data structure where the default parameters which correspond to the "funname" function have been set. For example,</para>
        <programlisting role="example"><![CDATA[
options = optimoptions("fmincon")
 ]]></programlisting>
        <para>returns a new data structure where the default parameters which correspond to the "fmincon" function have been set.</para>
        <para>The following syntax</para>
        <programlisting role='no-scilab-exec'><![CDATA[
options = optimoptions(oldoptions, key, value)
 ]]></programlisting>
        <para>creates a new data structure where all fields from the "oldoptions"
            structure have been copied, except the field corresponding to the "key",
            which has been set to "value".
        </para>
    </refsection>
    <refsection>
        <title>Arguments</title>
        <variablelist>
            <varlistentry>
                <term>key</term>
                    <para>a string. See below the typical values</para>
            </varlistentry>
            <varlistentry>
                <term>value</term>
                <listitem>
                    <para>
                        the value of the parameter
                    </para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>options</term>
                <listitem>
                    <para>
                        A data structure which contains the following fields.
                       Specific settinggs are associated with a particular function name.
                    </para>
                    <variablelist>
                        <varlistentry>
                            <term>options.Display</term>
                            <listitem>
                                <para>
                                    The verbosity level.
                                    The default value is "notify".
                                    The following is a list of available verbosity levels.
                                </para>
                                <itemizedlist>
                                    <listitem>
                                        <para>
                                            options.Display = "off": the algorithm displays no message at all.
                                        </para>
                                    </listitem>
                                    <listitem>
                                        <para>
                                            options.Display = "notify": the algorithm displays message if the termination criteria
                                            is not reached at the end of the optimization. This may happen if the
                                            maximum number of iterations of the maximum number of function evaluations
                                            is reached and warns the user of a convergence problem.
                                        </para>
                                    </listitem>
                                    <listitem>
                                        <para>
                                            options.Display = "final": the algorithm displays a message at the end of the optimization,
                                            showing the number of iterations, the number of function evaluations
                                            and the status of the optimization.
                                            This option includes the messages generated by the "notify" option i.e. warns
                                            in case of a convergence problem.
                                        </para>
                                    </listitem>
                                    <listitem>
                                        <para>
                                            options.Display = "iter": the algorithm displays a one-line message at each iteration.
                                            This option includes the messages generated by the "notify" option i.e. warns
                                            in case of a convergence problem. It also includes the message generated
                                            by the "final" option.
                                        </para>
                                    </listitem>
                                </itemizedlist>
                            </listitem>
                        </varlistentry>
                        <varlistentry>
                            <term>options.MaxFunctionEvaluations</term>
                            <listitem>
                                <para>
                                    The maximum number of evaluations of the cost function.
                                </para>
                            </listitem>
                        </varlistentry>
                        <varlistentry>
                            <term>options.MaxIterations</term>
                            <listitem>
                                <para>
                                    The maximum number of iterations.
                                </para>
                            </listitem>
                        </varlistentry>
                        <varlistentry>
                            <term>options.OutputFcn</term>
                            <listitem>
                                <para>
                                    A function which is called at each iteration to print out intermediate
                                    state of the optimization algorithm (for example into a log file).
                                </para>
                            </listitem>
                        </varlistentry>
                        <varlistentry>
                            <term>options.PlotFcn</term>
                            <listitem>
                                <para>
                                    A function which is called at each iteration to plot the intermediate
                                    state of the optimization algorithm (for example into a 2D graphic).
                                </para>
                            </listitem>
                        </varlistentry>
                        <varlistentry>
                            <term>options.FunctionTolerance</term>
                            <listitem>
                                <para>
                                    The absolute tolerance on function value.
                                </para>
                            </listitem>
                        </varlistentry>
                        <varlistentry>
                            <term>options.OptimalityTolerance</term>
                            <listitem>
                                <para>
                                    The absolute tolerance on the first order optimality.
                                </para>
                            </listitem>
                        </varlistentry>
                    </variablelist>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>funname</term>
                <listitem>
                    <para>
                        A string containing the name of an optimization function which
                        takes the options structure as input argument. Currently, the only
                        possible value is "fmincon".
                    </para>
                </listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Design</title>
        <para>
            Most optimization algorithms require many algorithmic
            parameters such as the number of iterations or the number of
            function evaluations. If these parameters are given to the optimization
            function as input parameters, this forces both the user and the
            developer to manage many input parameters. For example, the "optim" function
            provides more than 20 input arguments. The goal of the optimoptions function
            is to simplify the management of input arguments, by gathering
            all the parameters into a single data structure.
        </para>
        <para>
            While the current implementation of the "optimoptions" function only supports
            the fmincon function, it is designed to be extended to as many
            optimization function as required. Because all optimization algorithms
            do not require the same parameters, the data structure aims at remaining
            flexible. But, most of the time, most parameters are the same from
            algorithm to algorithm, for example, the tolerance parameters which
            drive the termination criteria are often the same, even if the
            termination criteria itself is not the same.
        </para>
    </refsection>
    <refsection>
        <title>Output and plot functions</title>
        <para>
            The "OutputFcn" and "PlotFcn" options accept as argument a function (or a list
            of functions). In the client optimization algorithm, this output or plot function
            is called back once per iteration. It can be used by the user
            to display a message in the console, write into a file, or eventually stop the algorithm.
        </para>
        <para>
            The output or plot function is expected to have the following prototype:
        </para>
        <programlisting role="no-scilab-exec"><![CDATA[
stop = myfun(x, optimValues, state)
 ]]></programlisting>
        <para>where the input parameters are:</para>
        <itemizedlist>
            <listitem>
                <para>
                    <literal>x</literal>: the current point
                </para>
            </listitem>
            <listitem>
                <para>
                    <literal>optimValues</literal>: a <literal>struct</literal> which contains the following fields
                </para>
                <itemizedlist>
                    <listitem>
                        <para>
                            <literal>optimValues.funccount</literal>: the number of function evaluations
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <literal>optimValues.fval</literal>: the best function value
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <literal>optimValues.iteration</literal>: the current iteration number
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            <literal>optimValues.procedure</literal>: the type of step performed.
                        </para>
                    </listitem>
                </itemizedlist>
            </listitem>
            <listitem>
                <para>
                    <literal>state</literal>: the state of the algorithm. The following states are available : "init", "iter" and "done".
                </para>
                <itemizedlist>
                    <listitem>
                        <para>
                            "init", when the algorithm is initializing,
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            "iter", when the algorithm is performing iterations,
                        </para>
                    </listitem>
                    <listitem>
                        <para>
                            "done", when the algorithm is terminated.
                        </para>
                    </listitem>
                </itemizedlist>
            </listitem>
            <listitem>
                <para>
                    <literal>stop</literal>: for general OutputFcn setting <literal>stop</literal> to <code>%t</code> will stop the algorithm (the output is ignored for PlotFcn).
                </para>
              </listitem>       
        </itemizedlist>
    </refsection>
    <refsection>
        <title>Example</title>
        <para>
            In the following example, we create an optimization structure
            with all fields set to the default settings for the "fmincon"
            optimization function.
        </para>
        <programlisting role="example"><![CDATA[
function stop = myoutputfun(x, optimValues, state)
  stop = %f;
endfunction

function myplotfun(x, optimValues, state)
  plot(x)
endfunction

opt = optimoptions ("fmincon", "Display","iter",...
               "MaxFunctionEvaluations",100,...
               "MaxIterations",110,...
               "OutputFcn",myoutputfun,...
               "PlotFcn",myplotfun,...
               "FunctionTolerance",1.e-12)
 ]]></programlisting>
    </refsection>
    <refsection role="see also">
        <title>See also</title>
        <simplelist type="inline">
            <member>
                <link linkend="fmincon">fmincon</link>
            </member>
        </simplelist>
    </refsection>
</refentry>
