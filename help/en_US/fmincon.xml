<?xml version='1.0' encoding='UTF-8'?>

<!--
* 
* This help file was generated from fmincon.sci using help_from_sci().
* 
-->

<refentry version='5.0-subset Scilab' xml:id='fmincon' xml:lang='en'
  xmlns='http://docbook.org/ns/docbook'
  xmlns:xlink='http://www.w3.org/1999/xlink'
  xmlns:svg='http://www.w3.org/2000/svg'
  xmlns:ns3='http://www.w3.org/1999/xhtml'
  xmlns:mml='http://www.w3.org/1998/Math/MathML'
  xmlns:db='http://docbook.org/ns/docbook'>

  <info>
    <pubdate>$LastChangedDate: 02-Sep-2010 $</pubdate>
  </info>

  <refnamediv>
    <refname>fmincon</refname><refpurpose>Solves a nonlinearily constrained optimization problem.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      x = fmincon(objective,x0)
      x = fmincon(objective,x0,Aineq,bineq)
      x = fmincon(objective,x0,Aineq,bineq,Aeq,beq)
      x = fmincon(objective,x0,Aineq,bineq,Aeq,beq,lb,ub)
      x = fmincon(objective,x0,Aineq,bineq,Aeq,beq,lb,ub,nonlcon)
      x = fmincon(objective,x0,Aineq,bineq,Aeq,beq,lb,ub,nonlcon,options)
      x = fmincon(problem)
      [x,fval,exitflag,output,lambda,grad,hessian] = fmincon ( ... )
   
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry><term>objective:</term>
      <listitem><para> a function, the function to minimize. See below for the complete specifications.</para></listitem></varlistentry>
      <varlistentry><term>x0:</term>
      <listitem><para> a nx1 or 1xn matrix of doubles, where n is the number of variables. The initial guess for the optimization algorithm.</para></listitem></varlistentry>
      <varlistentry><term>Aineq:</term>
      <listitem><para> a nil x n matrix of doubles, where n is the number of variables and nil is the number of linear inequalities. If Aineq==[] and bineq==[], it is assumed that there is no linear inequality constraints. If (Aineq==[] &amp; bineq&lt;&gt;[]), fmincon generates an error (the same happens if (Aineq&lt;&gt;[] &amp; bineq==[])).</para></listitem></varlistentry>
      <varlistentry><term>bineq:</term>
      <listitem><para> a nil x 1 matrix of doubles, where nil is the number of linear inequalities.</para></listitem></varlistentry>
      <varlistentry><term>Aeq:</term>
      <listitem><para> a nel x n matrix of doubles, where n is the number of variables and nel is the number of linear equalities.  If Aeq==[] and beq==[], it is assumed that there is no linear equality constraints. If (Aeq==[] &amp; beq&lt;&gt;[]), fmincon generates an error (the same happens if (Aeq&lt;&gt;[] &amp; beq==[])).</para></listitem></varlistentry>
      <varlistentry><term>beq:</term>
      <listitem><para> a nel x 1 matrix of doubles, where nel is the number of linear inequalities.</para></listitem></varlistentry>
      <varlistentry><term>lb:</term>
      <listitem><para> a nx1 or 1xn matrix of doubles, where n is the number of variables. The lower bound for x. If lb==[], then the lower bound is automatically set to -inf.</para></listitem></varlistentry>
      <varlistentry><term>ub:</term>
      <listitem><para> a nx1 or 1xn matrix of doubles, where n is the number of variables. The upper bound for x. If lb==[], then the upper bound is automatically set to +inf.</para></listitem></varlistentry>
      <varlistentry><term>nonlcon:</term>
      <listitem><para> a function, the nonlinear constraints. See below for the complete specifications.</para></listitem></varlistentry>
      <varlistentry><term>x:</term>
      <listitem><para> a nx1 matrix of doubles, the computed solution of the optimization problem</para></listitem></varlistentry>
      <varlistentry><term>fval:</term>
      <listitem><para> a 1x1 matrix of doubles, the function value at x</para></listitem></varlistentry>
      <varlistentry><term>exitflag:</term>
      <listitem><para> a 1x1 matrix of floating point integers, the exit status. See below for details.</para></listitem></varlistentry>
      <varlistentry><term>output:</term>
      <listitem><para> a struct, the details of the optimization process.  See below for details.</para></listitem></varlistentry>
      <varlistentry><term>lambda:</term>
      <listitem><para> a struct, the Lagrange multipliers at optimum.  See below for details.</para></listitem></varlistentry>
      <varlistentry><term>grad:</term>
      <listitem><para> a nx1 matrix of doubles, the gradient of the objective function at optimum</para></listitem></varlistentry>
      <varlistentry><term>hessian:</term>
      <listitem><para> a nxn matrix of doubles, the Hessian of the objective function at optimum</para></listitem></varlistentry>
      <varlistentry>
        <term>options:</term>
        <listitem>
          <para>a structure which handles parameters allowing a fine
            tuning of fmincon (see the dedicated section below).</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>problem:</term>
          <listitem>
            <para>A structure with fields having the same name and meaning as the formal input parameters described below. Omitting a field amounts to specify an empty matrix in the long syntax.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </refsection>

    <refsection>
      <title>Description</title>
      <para>
        Search the minimum of a constrained optimization problem specified by :
        find the minimum of f(x) such that
      </para>
      <para>
        c(x)&lt;=0, ceq(x)&lt;=0, A*x&lt;=b, Aeq*x=beq and lb&lt;=x&lt;=ub.
      </para>
      <para>
        <latex>
          \begin{eqnarray}
          \mbox{min}_{x}    &amp; f(x) \\
          \mbox{subject to} &amp; c(x) \leq 0 \\
          &amp; c_{eq}(x) = 0 \\
          &amp; Ax \leq b \\
          &amp; A_{eq} x = b_{eq} \\
          &amp; lb \leq x \leq ub
          \end{eqnarray}
        </latex>
      </para>
      <para>
        Currently, we use ipopt for the actual solver of fmincon.
      </para>
      <para>
        See the demonstrations for additionnal examples.
      </para>
      <para>
        The objective function must have header :
        </para><para><programlisting role='no-scilab-exec'>
          f = objfun(x)
        </programlisting></para><para>
        where x is a n x 1 matrix of doubles and f is a 1 x 1 matrix of doubles.
        On input, the variable x contains the current point and, on output,
        the variable f must contain the objective function value.
      </para>
      <para>
        By default, fmincon uses finite differences with order 2 formulas and
        optimum step size in order to compute a numerical gradient of the
        objective function.
        If we can provide exact gradients, we should do so since it improves
        the convergence speed of the optimization algorithm.
        In order to use exact gradients, we must update the header of the
        objective function to :
        </para><para><programlisting role='no-scilab-exec'>
          [f,g] = objfungrad(x)
        </programlisting></para><para>
        where x is a n x 1 matrix of doubles, f is a 1 x 1 matrix of doubles
        and g is a n x 1 matrix of doubles.
        On input, the variable x contains the current point and, on output,
        the variable f must contain the objective function value and the variable
        g must contain the gradient of the objective function.
        Furthermore, we must enable the 'SpecifyObjectiveGradient' option with the statement:
        </para><para><programlisting role='no-scilab-exec'>
          options = optimoptions('fmincon','SpecifyObjectiveGradient', %t);
        </programlisting></para><para>
        This will let fmincon know that the exact gradient of the objective
        function is known, so that it can change the calling sequence to the
        objective function.
      </para>
      <para>
        The constraint function must have header :
        </para><para><programlisting role='no-scilab-exec'>
          [c, ceq] = confun(x)
        </programlisting></para><para>
        where x is a n x 1 matrix of doubles, c is a nni x 1 matrix of doubles and
        ceq is a nne x 1 matrix of doubles (nni : number of nonlinear inequality
        constraints, nne : number of nonlinear equality constraints).
        On input, the variable x contains the current point and, on output,
        the variable c must contain the nonlinear inequality constraints and ceq must contain the
        nonlinear equality constraints.
      </para>
      <para>
        By default, fmincon uses finite differences with order 2 formulas and
        optimum step size in order to compute a numerical gradient of the
        constraint function.
        In order to use exact gradients, we must update the header of the
        constraint function to :
        </para><para><programlisting role='no-scilab-exec'>
          [c,ceq,DC,DCeq] = confungrad(x)
        </programlisting></para><para>
        where x is a n x 1 matrix of doubles, c is a nni x 1 matrix of doubles,
        ceq is a nne x 1 matrix of doubles, DC is a n x nni matrix of doubles and
        DCeq is a n x nne matrix of doubles.
        On input, the variable x contains the current point and, on output,
        the variable c must contain the nonlinear inequality constraint function value,
        the variable ceq must contain the nonlinear equality constraint function value,
        the variable DC must contain the transpose of Jacobian matrix of the nonlinear inequality constraints
        and the variable DCeq must contain the transpose Jacobian matrix of the nonlinear equality constraints.
        The gradient of the i-th nonlinear inequality constraint is is stored in DC(:,i) (same for DCeq).
        Furthermore, we must enable the 'SpecifyConstraintGradient' option with the statement :
        </para><para><programlisting role='no-scilab-exec'>
          options = optimsoptions('fmincon','SpecifyConstraintGradient',%t);
        </programlisting></para><para>
      </para>
      <para>
        By default, fmincon uses a L-BFGS formula to compute an
        approximation of the Hessian of the Lagrangian.
        Notice that this is different from Matlab's fmincon, which
        default is to use a BFGS update.
      </para>
      <para>
        The exitflag variable allows to know the status of the optimization.
        <itemizedlist>
          <listitem>exitflag = 0 : Number of iterations exceeded options.MaxIter or number of function evaluations exceeded options.FunEvals.</listitem>
          <listitem>exitflag = 1 : First-order optimality measure was less than options.TolFun, and maximum constraint violation was less than options.TolCon.</listitem>
          <listitem>exitflag = -1 : The output function terminated the algorithm.</listitem>
          <listitem>exitflag = -2 : No feasible point was found.</listitem>
          <listitem>exitflag = %nan : Other type of termination.</listitem>
        </itemizedlist>
      </para>
      <para>
        The output data structure contains detailed informations about the
        optimization process.
        It has type 'struct' and contains the following fields.
        <itemizedlist>
          <listitem>output.iterations: the number of iterations performed during the search</listitem>
          <listitem>output.funcCount: the number of function evaluations during the search</listitem>
          <listitem>output.stepsize: an empty matrix</listitem>
          <listitem>output.algorithm : the string containing the name of the algorithm. In the current version, algorithm='ipopt'.</listitem>
          <listitem>output.firstorderopt: the max-norm of the first-order KKT conditions.</listitem>
          <listitem>output.constrviolation: the max-norm of the constraint violation.</listitem>
          <listitem>output.cgiterations: the number of preconditionned conjugate gradient steps. In the current version, cgiterations=0.</listitem>
          <listitem>output.message: a string containing a message describing the status of the optimization.</listitem>
        </itemizedlist>
      </para>
      <para>
        The lambda data structure contains the Lagrange multipliers at the
        end of optimization.
        It has type 'struct' and contains the following
        fields.
        <itemizedlist>
          <listitem>lambda.lower: the Lagrange multipliers for the lower bound constraints. In the current version, an empty matrix.</listitem>
          <listitem>lambda.upper: the Lagrange multipliers for the upper bound constraints. In the current version, an empty matrix.</listitem>
          <listitem>lambda.eqlin: the Lagrange multipliers for the linear equality constraints.</listitem>
          <listitem>lambda.eqnonlin: the Lagrange multipliers for the nonlinear equality constraints.</listitem>
          <listitem>lambda.ineqlin: the Lagrange multipliers for the linear inequality constraints.</listitem>
          <listitem>lambda.ineqnonlin: the Lagrange multipliers for the nonlinear inequality constraints.</listitem>
        </itemizedlist>
      </para>
    </refsection>

    <refsection>
      <title>Example</title>
      <programlisting role='example'><![CDATA[
        // A basic case :
        // we provide only the objective function and the nonlinear constraint
        // function : we let fmincon compute the gradients by numerical
        // derivatives.
        function f = objfun ( x )
          f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1)
        endfunction

        function [c, ceq] = confun(x)
        // Nonlinear inequality constraints
        c = [ 1.5 + x(1)*x(2) - x(1) - x(2)
             -x(1)*x(2) - 10 ]
        // Nonlinear equality constraints
        ceq = []
        endfunction

        // The initial guess
        x0 = [-1,1];

        // The expected solution : only 4 digits are guaranteed
        xopt = [-9.547345885974547   1.047408305349257]
        fopt = 0.023551460139148

        // Run fmincon
        [x,fval,exitflag,output,lambda,grad,hessian] = ..
        fmincon (objfun,x0,[],[],[],[],[],[], confun)

        ]]></programlisting>
      </refsection>

        <refsection>
          <title>Example</title>
          <programlisting role='example'><![CDATA[
            // A case where we set the bounds of the optimization.
            // By default, the bounds are set to infinity.
            function f = objfun ( x )
              f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1)
            endfunction

            function [c, ceq] = confun(x)
              // Nonlinear inequality constraints
              c = [ 1.5 + x(1)*x(2) - x(1) - x(2)
                   -x(1)*x(2) - 10 ]
              // Nonlinear equality constraints
              ceq = []
            endfunction

            // The initial guess
            x0 = [-1,1];

            // The expected solution
            xopt = [0   1.5]
            fopt = 8.5

            // Make sure that x(1)>=0, and x(2)>=0
            lb = [0,0];
            ub = [ ];

            // Run fmincon
            [x,fval] = fmincon ( objfun , x0,[],[],[],[],lb,ub,confun)

            ]]></programlisting>
          </refsection>

          <refsection>
            <title>Same example with a problem structure</title>
            <programlisting role='example'><![CDATA[
              function f = objfun ( x )
                f = exp(x(1))*(4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1)
              endfunction

              function [c, ceq] = confun(x)
                c = [ 1.5 + x(1)*x(2) - x(1) - x(2)
                     -x(1)*x(2) - 10]
                ceq = []
              endfunction

              // Define the problem structure
              problem = struct();
              problem.x0 = [-1,1];
              problem.lb = [0,0];
              problem.objective = objfun;
              problem.nonlcon = confun;

              // Run fmincon
              [x,fval] = fmincon(problem)

              ]]></programlisting>
            </refsection>

            <refsection>
              <title>Same example with Hessian and derivatives check</title>
              <programlisting role='example'><![CDATA[
                // A case where we provide the gradient of the objective
                // function and the Jacobian matrix of the constraints.

                // The objective function and its gradient
                function [f,g]=objfun(x)
                  p = 4*x(1)^2 + 2*x(2)^2 + 4*x(1)*x(2) + 2*x(2) + 1;
                  e = exp(x(1));
                  f = e*p;
                  if argn(1) == 2
                      g = e*[p+8*x(1)+4*x(2); 4*x(2)+4*x(1)+2];
                  end
                endfunction

                // The nonlinear constraints and their gradients
                function [c, ceq, dc,dceq]=confun(x)
                  // Inequality constraints
                  c = [ 1.5+x(1)*x(2)-x(1)-x(2)
                       -x(1)*x(2)-10]
                  // No inequality constraint
                  ceq = [];
                  if argn(1) == 4
                    // dc(:,i) = gradient of the i-th constraint
                    // dc is the transpose of the Jacobian of constraints
                    dc = [x(2)-1, -x(2)
                          x(1)-1, -x(1)];
                    dceq = [];
                  end 
                endfunction

                // The Hessian of the Lagrangian
                function h = hessfun(x,lambda)
                    l = lambda.ineqnonlin;
                    e = exp(x(1));
                    h(1,1) = e*(16*x(1)+10*x(2)+4*x(1)*x(2)+4*x(1)^2+2*x(2)^2+9);
                    h(1,2) = e*(6+4*x(1)+4*x(2))+l(1)-l(2);
                    h(2,2) = 4*e;
                    h(2,1) = h(1,2);
                endfunction

                // Define the problem structure
                problem = struct();
                problem.x0 = [-1,1];
                problem.lb = [0,0];
                problem.objective = objfun;
                problem.nonlcon = confun;
                // Optimization options
                problem.options = optimoptions("fmincon","Display","iter",...
                "SpecifyObjectiveGradient",%t,"SpecifyConstraintGradient",%t,...
                "CheckGradients",%t,"HessianFcn",hessfun)

                // Run fmincon
                [x,fval] = fmincon(problem)
                ]]></programlisting>
              </refsection
              >
              <refsection>
                <title>Example with Hessian output in objective</title>
                <programlisting role='example'><![CDATA[
                  function [f, g, H] = rosenfgh(x)
                      f = 100*(x(2) - x(1)^2)^2 + (1-x(1))^2;
                      if argn(1) > 1 // g required
                          g = [-400*(x(2)-x(1)^2)*x(1)-2*(1-x(1));
                              200*(x(2)-x(1)^2)];
                          if argn(1) > 2// H required
                              H = [1200*x(1)^2-400*x(2)+2, -400*x(1);
                                  -400*x(1), 200];  
                          end
                      end
                  end

                  opt=optimoptions("fmincon");
                  opt.SpecifyObjectiveGradient = %t;
                  opt.Display = "iter";
                  opt.HessianFcn = "objective";

                  problem = struct();
                  problem.objective = rosenfgh;
                  problem.x0 = [0;0];
                  problem.options = opt;

                  x=fmincon(problem)
                  ]]></programlisting>
                </refsection>

            <refsection>
              <title>fmincon options</title>
              <para>The default option structure for fmincon can be obtained as the output of optimoptions('fmincon'). General information about fields which are not specfic to fmincon can be found on the dedicated page <link linkend='optimoptions'>optimoptions</link>.
            </para>
            <para> 
              <informaltable border='1'>
                <tbody>
                  <tr>
                    <td>Algorithm</td>
                    <td>
                      <para>
                        Optimization algorithm. This version of fmincon only allows the value 'ipopt'.  
                      </para>
                    </td>
                  </tr>
                  <tr><td>CheckGradients</td>
                  <td>
                    <para>
                      Set this field to %t to check user provided gradients (objective and constraints) before starting the optimization algorithm.
                    </para>
                  </td>
                </tr>
                <tr><td>ConstraintTolerance</td>
                <td>
                  <para>
                    Absolute tolerance on the constraint violation.
                    Successful termination requires that the max-norm of the
                    constraint violation is less than this threshold. The valid range for
                    this real option is 0 &lt; ConstraintTolerance &lt; +inf and its default
                    value is 0.0001.
                  </para>
                </td>
              </tr>
              <tr><td>FiniteDifferenceStepSize</td>
              <td>
                <para>
                  this is the relative stepsize (see also 'TypicalX' below). For a given <code>x</code> the actual step is </para>
                        <para/>
                        <para>
                          <code>sign(x).*FiniteDifferenceStepSize.*max(TypicalX,x)</code>
                        </para>
                        <para/>
                        <para>It can be a scalar, or a vector with the same size as <code>x</code>. Its default value depends on the chosen approximation scheme: <code>sqrt(%eps)</code> for the 'forward' scheme, <code>%eps^(1/3)</code> for the 'centered' scheme and <code>1e-100</code> for the 'complexStep' scheme.</para>
              </td>
            </tr>
            <tr><td>FiniteDifferenceType</td>
            <td>
              <para>This field gives the chosen approximation scheme. The possible values are</para>
                
                <simplelist type='vert'>
                  <member> 'forward' (the usual forward order 1 scheme)</member>
                  <member>'centered' (the default order 2 centered scheme)</member>
                  <member>'complexStep' (the order 2 complex step scheme)</member>
                </simplelist>
                <para>If you choose the complex step scheme check that objective and constraints function accepts complex input and handles it correctly (for example transposition operator has to be the dot-prefixed non-conjugate transposition <code>.'</code>).
            </para>
            </td>
          </tr>
          <tr><td>SpecifyObjectiveGradient</td>
          <td>
            <para>When SpecifyObjectiveGradient is set to <code>%t</code> the objective function must have the prototype 
          </para><para><code>[f,g] = objfungrad(x)</code></para><para> and yield the exact gradient of the objective function as second output argument. When SpecifyObjectiveGradient is <code>%f</code> (the default) the gradient is approximated by finite differences.
            </para>
          </td>
        </tr>
        <tr><td>SpecifyConstraintGradient</td>
        <td>
          <para>When SpecifyConstraintGradient is set to <code>%t</code> the constraints function must have the prototype </para><para><code>[c,ceq,dc,dceq] = constrgrad(x)</code></para><para> and yield the exact gradient (transpose of the Jacobian) of the inequality and inequality constraint as third and fourth output argument. When SpecifyConstraintGradient is <code>%f</code> (the default) these gradients are approximated by finite differences.
          </para>
        </td>
      </tr>
          <tr>
            <td>StepTolerance</td>
            <td>
              <para>
                Tolerance for detecting
                        numerically insignificant steps. If the search direction in the primal
                        variable is, in relative terms for each component, less
                        than this value, the algorithm accepts the full step without line
                        search. If this happens repeatedly, the algorithm will terminate with
                        a corresponding exit message. Its default value is 10.0*%eps.
                      </para>
            </td>
        </tr>
          <tr><td>TypicalX</td>
          <td>
          <para>A vector of the same size as <code>x0</code>, with typical magnitude of components. The default value is <code>ones(x0)</code>.</para>
        </td>
        </tr>
          <tr>
            <td>ScaleProblem</td>
            <td>
              <para>
                Select the technique used for scaling the optimization problem. Possible values are 'gradient-based' (the default) and 'none'.
              </para>
            </td>
        </tr>
          <tr><td>HessianApproximation</td>
          <td>
            <para>Defines the type of Hessian approximation. Possible values are</para> 
              <simplelist type='vert'>
                <member> 'bfgs' for classical BFGS update formula</member>
                <member>{'lbfgs',n} for limited-memory BFGS update (default value for n is 6)</member>
                <member>'finite-difference'</member>
              </simplelist>
              <para>The last value is allowed only if the exact gradients of objective and constraints are given (see SpecifyConstraintGradient and SpecifyObjectiveGradient below). In that case, and if the Hessian is known to be sparse, consider using the HessPattern option.
            </para>
          </td>
        </tr>
        <tr>
          <td>HessianFcn</td>
          <td>
            <para>In the general case this field defines a function computing the Hessian of the Lagragian with the prototype </para><para><code>H = hessian(x,lambda)</code></para><para> When there are only bounds or linear constraints then the Hessian of the Lagrangian is the Hessian of the objective. In that case the  HessianFcn field can take the value 'objective' and the objective prototype has to be </para><para><code>[f,g,H] = objective(x)</code></para><para> (and SpecifyObjectiveGradient must be set to <code>%t</code>).
            </para>
          </td>
        </tr>
        <tr>
          <td>HessPattern</td>
          <td>
            <para>Defines the sparsity pattern of the Hessian and allows to reduce the number of objective and gradient evaluations when HessianApproximation is equal to 'finite-difference'. The HessPattern field can be a sparse matrix or a sparse boolean matrix where non-zero (or %t) elements define the non-zero second derivatives.
            </para>
          </td>
        </tr>
        <tr>
          <td>HonorBounds</td>
          <td>
            <para>
              If this field is <code>%f</code>, the bounds given by the user may be slightly relaxed (typical factor is <code>sqrt(%eps)</code>). The default value is <code>%t</code>.
            </para>
          </td>
        </tr>
        <!--        <tr><td>ObjectiveLimit</td></tr>-->
        <tr>
          <td>OptimalityTolerance</td>
          <td>
            <para>
              Determines the convergence tolerance for the algorithm.
              The algorithm terminates successfully, if the scaled NLP error
              becomes smaller than this value. The valid range for this real option is 0 &lt; OptimalityTolerance
              &lt; +inf and its default value is 1.10^-08.
            </para>
          </td>
        </tr>
        <tr>
          <td>InitBarrierParam</td>
        <td>
          <para>
            Initial value for the barrier
                parameter. This option determines the initial value for the barrier
                parameter (mu). It is only relevant in the monotone,
                version of the algorithm (i.e., if 'BarrierParamUpdate' is chosen as
                'monotone'). The valid range for this real option is 0 &lt; InitBarrierParam
                &lt; +inf and its default value is 0.1.
              </para>
            </td>
          </tr>
        <tr><td>BarrierParamUpdate</td>
        <td>
          <para> Update strategy for barrier
        parameter. Determines which barrier parameter update strategy is to be
        used. Possible values are 'monotone' (the default) or 'adaptive'.
      </para>
      </td>
      </tr>
        <tr><td>OutputFcn</td>
        <td><para>See <link linkend='optimoptions'>optimoptions</link></para></td>
      </tr>
        <tr><td>PlotFcn</td>
        <td><para>See <link linkend='optimoptions'>optimoptions</link></para></td>
      </tr>
      </tbody>
    </informaltable>
  </para>
</refsection>


<refsection>
  <title>See Also</title>
  <simplelist type='inline'>
    <member><link linkend='optimoptions'>optimoptions</link></member>
  </simplelist>
</refsection>
<refsection>
  <title>Authors</title>
  <simplelist type='vert'>
    <member>Michael Baudin, DIGITEO, 2010</member>
    <member>Stéphane Mottelet, 2021</member>
  </simplelist>
</refsection>
</refentry>
