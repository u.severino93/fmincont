// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of fmincon toolbox
//
// Copyright (C) 2020-2021 Stéphane Mottelet
//
// This file must be used under the terms of GPL License
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// https://www.gnu.org/licenses/gpl-3.0.txt

path_makedist = get_absolute_file_path("makedist.sce");
cd(path_makedist);

deletefile(".gitignore");
[sci,v]=getversion()
filename = "fmincon_"+mgetl("VERSION")+".bin.zip"
if getos() <> "Windows" then
    cd ..
    movefile("fmincont/makedist.sce","makedist.sce")
    unix_g("zip -r "+filename+" fmincont")
else
    cd ..
    movefile("fmincont/makedist.sce","makedist.sce")
    host(SCI+"/tools/zip/zip.exe -r "+filename+" fmincont")
end
movefile("makedist.sce","fmincont/makedist.sce")
cd(path_makedist);
