// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Returns the string containing the fmincon/FSQP data structure.
//
// Calling Sequence
//   str = %TFSQP_string ( dstr )
//
// Parameters
//   s: the fmincon data structure
//   str: a matrix of strings
//
// Description
//   Returns the string containing the fmincon/FSQP data structure.
//
// Authors
// Michael Baudin, DIGITEO, 2010

function str = %TFSQP_string ( dstr )

  k = 1
  str = []
  // Add fun
  if ( _mlist_isfield ( s , "fun" ) ) then 
    str(k) = "fun=<function>"
    k = k + 1
  end
  // Add nonlcon
  if ( _mlist_isfield ( s , "nonlcon" ) ) then 
    str(k) = "nonlcon=<function>"
    k = k + 1
  end
  // Add x0
  if ( _mlist_isfield ( s , "x0" ) ) then 
    str(k) = "x0=["+_strvec ( s.x0 )+"]''"
    k = k + 1
  end
  // Add A
  if ( _mlist_isfield ( s , "A" ) ) then 
    str(k) = "A= "
    k = k + 1 
    sA = _strmat ( A )
    nrows = size(s.A,"r")
    str(k:k+nrows-1) = sA
    k = k + nrows
  end
  // Add b
  if ( _mlist_isfield ( s , "b" ) ) then 
    str(k) = "b=["+_strvec ( s.b )+"]''"
    k = k + 1
  end
  // Add Aeq
  if ( _mlist_isfield ( s , "Aeq" ) ) then 
    str(k) = "Aeq= "
    k = k + 1 
    sA = _strmat ( Aeq )
    nrows = size(s.Aeq,"r")
    str(k:k+nrows-1) = sA
    k = k + nrows
  end
  // Add beq
  if ( _mlist_isfield ( s , "beq" ) ) then 
    str(k) = "beq=["+_strvec ( s.beq )+"]''"
    k = k + 1
  end
  // Add lb
  if ( _mlist_isfield ( s , "lb" ) ) then 
    str(k) = "lb=["+_strvec ( s.lb )+"]''"
    k = k + 1
  end
  // Add ub
  if ( _mlist_isfield ( s , "ub" ) ) then 
    str(k) = "ub=["+_strvec ( s.ub )+"]''"
    k = k + 1
  end
  // Add verbose
  if ( _mlist_isfield ( s , "verbose" ) ) then 
    str(k) = "verbose="+_strvec ( s.verbose )
    k = k + 1
  end
  // Add options
  if ( _mlist_isfield ( s , "options" ) ) then 
    str(k) = "options Object"
    k = k + 1
  end
  // Add options
  if ( _mlist_isfield ( s , "storeCntr" ) ) then 
    str(k) = "storeCntr"+_strvec ( s.storeCntr )
    k = k + 1
  end
endfunction
  //
  // _strvec --
  //  Returns a string for the given vector.
  //
  function str = _strvec ( x )
    str = strcat(string(x)," ")
  endfunction
  //
  // _strmat --
  //  Returns a column vector of rows with the matrix A inside
  //
  function str = _strmat ( A )
    if ( A == [] ) then
      str = "[]"
      return
    end
    nrows = size(s.A,"r")
    ncols = size(s.A,"c")
    k = 1
    sA = string(A)
    for i = 1 : nrows
      str(k) = ""
      str(k) = str(k) + sA(i,1)
      for j = 2 : ncols
        str(k) = str(k) + " " + sA(i,j)
      end
      k = k + 1 
    end
  endfunction

function bool = _mlist_isfield ( s , fieldname ) 
  // Get the matrix of integers representing defined fields
  df = definedfields ( s )
  // Search for the index ifield associated with given fieldname
  ifield = find(s(1)==fieldname)
  // Search for ifield in the matrix of defined fields
  jj = find(df==ifield)
  bool = jj <> []
endfunction

