function out = %optim_options_set_tolerance(prop,value,values)
    if typeof(value) <> "constant" then
        message = msprintf("Invalid value for %s option: a positive real is expected.",prop);
        error(message)       
    elseif value <= %eps
        message = msprintf("Invalid value for %s option: value cannot be less than relative machine precision %e.",prop,%eps);
        error(message)
    else
        out = value;              
    end
end