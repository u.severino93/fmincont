function out = %optim_options_set_sparse(prop,value,values)
    if or(typeof(value) == ["sparse","boolean sparse"])
        out = value;
    elseif typeof(value) == "constant"
        out = sparse(value);
    else
        error(msprintf("Invalid value for %s option: a sparse matrix is expected.",prop));
    end
end