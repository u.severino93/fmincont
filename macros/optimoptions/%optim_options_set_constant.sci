function out = %optim_options_set_constant(prop,value,values)
    if typeof(value) <> "constant"
        message = msprintf("Invalid value for %s option: a number is expected",prop);
        error(message)       
    elseif floor(values) == values // an integer expected
        if max(0,floor(value)) <> value
            message = msprintf("Invalid value for %s option: a positive integer is expected",prop);
            error(message)                   
        else
            out = value;
        end
    else
        out = value;
    end
end