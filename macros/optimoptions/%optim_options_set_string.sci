function out = %optim_options_set_string(prop,value,values)
    try
        k = find(convstr(value) == convstr(values));
    catch
        k = [];
    end
    if isempty(k) then
        message = msprintf("Invalid value for %s option: valid values are ",prop);
        for i = 1:size(values,"*")
            message = message + msprintf( """%s""",values(i));
            if i < size(values,"*")
                message = message + ","
            end
        end
        error(message)       
    else
        out = values(k);
    end
end