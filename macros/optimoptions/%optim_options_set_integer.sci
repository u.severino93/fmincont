function out = %optim_options_set_integer(prop,value,values)
    if typeof(value) <> "constant" || max(0,floor(value)) <> value
        message = msprintf("Invalid value for %s option: a positive integer is expected",prop);
        error(message)                   
    end
    out = value;
end