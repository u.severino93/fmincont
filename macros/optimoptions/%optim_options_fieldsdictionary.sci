function out = %optim_options_fieldsdictionary(opt)
    [st, sthid, solver] = %optim_options_defaults(opt)
    out = [fieldnames(st);fieldnames(sthid)];
endfunction