function out = %optim_options_set_boolean(prop,value,values)
    if typeof(value) <> "boolean" && (typeof(value) <> "constant" || value <> [0 1]) then
        message = msprintf("Invalid value for %s option: a boolean value is expected.",prop);
        error(message)       
    else
        out = (value+0)==1;
    end
end