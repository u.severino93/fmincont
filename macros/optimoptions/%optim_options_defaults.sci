function [st,sthid,solver,empty_content] = %optim_options_defaults(opt)
    empty_content = size(opt.contents,"*") == 0 || isempty(fieldnames(opt.contents))
    solver = strsubst(typeof(opt),"optim_options_","");
    if empty_content == %f && isfield(opt.contents,"Algorithm") then
        execstr(msprintf("[st,sthid] = %%optim_options_%s(""%s"")",solver,opt.contents.Algorithm));        
    else
        execstr(msprintf("[st,sthid] = %%optim_options_%s(""%s"")",solver,"default"));
    end
endfunction