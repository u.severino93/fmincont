function out = %optim_options_set_hessianapproximation(prop,value,values)
    if typeof(value) == "string" && size(value,"*") == 1
        if or(convstr(value)==["bfgs","lbfgs","finite-difference"])
            out = convstr(value)
           return
        end
    end
    if typeof(value) == "ce" && value{1} == "lbfgs" && typeof(value{2}) == "constant" && max(0,value{2}) == floor(value{2})
        out = value;                
    else
        message = msprintf("Invalid value for HessianApproximation option: valid values are ""bfgs"",""finite-difference"",""lbfgs"",{""lbfgs"",n}");
        error(message)
    end
end
