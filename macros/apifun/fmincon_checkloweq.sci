// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function errmsg = fmincon_checkloweq ( funname , var , varname , ivar , thr )
  // Checks that the value is lower or equal than a threshold.
  //
  // Calling Sequence
  //   errmsg = fmincon_checkloweq ( funname , var , varname , ivar , expectedopt )
  //
  // Parameters
  //   funname : a 1 x 1 matrix of strings, the name of the calling function.
  //   var : a 1 x 1 matrix of valid Scilab data type, the variable
  //   varname : a 1 x 1 matrix of string, the name of the variable
  //   ivar : a 1 x 1 matrix of floating point integers, the index of the input argument in the calling sequence
  //   thr : a matrix of values, the maximum value for the variable #ivar
  //   errmsg : a 1 x 1 matrix of strings, the error message. If there was no error, the error message is the empty matrix.
  //
  // Description
  // This function is designed to be used to design functions where an 
  // input argument is expected to be greater or equal to a threshold.
  // The error is generated if the condition or ( var > thr ) is true.
  // This function can be used for whatever variable type for which 
  // the comparison ">" can be evaluated.
  //
  // Examples
  // // The function takes an argument x such that x<=1.
  // function y = myfunction ( x )
  //   fmincon_checkloweq ( "myfunction" , x , "x" , 1 , 1 )
  //   y = sqrt(1-x)
  // endfunction
  // // Calling sequences which work
  // myfunction ( [-1.5,-2.5,-3.5] )
  // // Calling sequences which generate an error
  // myfunction ( [1.5,1] )
  // myfunction ( [1,-1,2.5,0] )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //

  errmsg = []
  if ( or ( var > thr ) ) then
    k = find ( var > thr )
    k = k(1)
    errmsg = msprintf(gettext("%s: Expected that all entries of input argument %s at input #%d are lower or equal than %s, but entry #%d is equal to %s."),funname,varname,ivar,string(thr),k,string(var(k)));
    error(errmsg);
  end
endfunction

