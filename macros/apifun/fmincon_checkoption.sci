// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function errmsg = fmincon_checkoption ( funname , var , varname , ivar , expectedopt )
  // Generates an error if the value of an input argument is not expected.
  //
  // Calling Sequence
  //   errmsg = fmincon_checkoption ( funname , var , varname , ivar , expectedopt )
  //
  // Parameters
  //   funname : a 1 x 1 matrix of strings, the name of the calling function.
  //   var : a 1 x 1 matrix of valid Scilab data type, the variable
  //   varname : a 1 x 1 matrix of string, the name of the variable
  //   ivar : a 1 x 1 matrix of floating point integers, the index of the input argument in the calling sequence
  //   expectedopt : a n x 1 or 1 x n matrix of values, the available values for the variable #ivar
  //   errmsg : a 1 x 1 matrix of strings, the error message. If there was no error, the error message is the empty matrix.
  //
  // Description
  // This function is designed to be used to design functions where an 
  // input argument has a limited number of possible values.
  //
  // Examples
  // // The function takes a string argument, either "r" or "c".
  // function myfunction ( x )
  //   fmincon_checkoption ( "myfunction" , x , "x" , 1 , ["r" "c"] )
  //   disp("This is the string:" + x)
  // endfunction
  // // Calling sequences which work
  // myfunction ( "r" )
  // myfunction ( "c" )
  // // Calling sequences which generate an error
  // myfunction ( "Scilab" )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //

  errmsg = []
  if ( and ( var <> expectedopt ) ) then
    stradd = """ or """
    strexp = """" + strcat(string(expectedopt),stradd) + """"
    errmsg = msprintf(gettext("%s: Expected value [%s] for input argument %s at input #%d, but got ""%s"" instead."),funname,strexp,varname,ivar,string(var));
    error(errmsg);
  end
endfunction

